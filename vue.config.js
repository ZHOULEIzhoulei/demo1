//webpack 配置 在vue项目中对于webpack补充或修改
// Vue.config.js 文件如果有修改，则一定要重新启动
// commom.js规范  给node js用的
// 服务代理  服务层==> 请求别人的服务层，没有跨域问题
// 此种方案只能用于开发阶段

module.exports = {
    // 服务器
    devServer:{
        // 指定vue项目开发阶段的端口号，之前默认为8080
        // port:9000

        // 代理配置
        proxy: {
            // /api就是在你ajax请求中写的参数1中的什么什么开头的标识
            '/api': {
              // 目标地址
              target: 'https://api.iynn.cn/film/api/v1',
              changeOrigin: true,
              pathRewrite: {
                '^/api':''
              }
            }
            
          }
    }
}
