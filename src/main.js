// 移入vue 类
import Vue from 'vue'
// 根组件
import App from './App'
// 路由主入口文件
import router from './router'
// 引入css重置默认样式
import './assets/css/reset.css'
// 引入字体图标iconfont
import'./assets/iconfont/iconfont.css'
// 引入全局组件  加载中...
import './components/Loading'

// 引入vant组件
import './plugins/vant'

// 引入图片懒加载组件
import './plugins/lazyload'

// 引入过滤器
import './config/filters'

// 事件总线对象
// 原则：先订阅，后发布
Vue.prototype.$eventBus = new Vue()

// 导入全局组件
// import './global/mylink'

// 运行提示消息是否打开
Vue.config.productionTip = false

// 实例化一个vue对象
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
