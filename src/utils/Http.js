import axios from 'axios'

class Http {
  constructor(opts = {}) {
    opts.timeout = opts.timeout || 5000
    if (opts.headers) {
      opts.headers.aa = opts.headers.aa || 'admin'
    } else {
      opts.headers = {
        aa: 'admin'
      }
    }
    // 添加到成员属性
    this.opts = opts
  }

  getInstance(opts = {}) {
    // 对象合并
    // let options = { ...this.opts, ...opts }
    let options = Object.assign({}, this.opts, opts)
    return axios.create(options)
  }

  get(url, opts = {}) {
    return this.getInstance(opts).get(url)
  }

  post(url, data = {}, opts = {}) {
    return this.getInstance(opts).post(url, data)
  }

}


export default new Http()
