
// 电影相关的网路请求uri地址配置
export default{

    // 正在热映
    getNowPlayingFilmList:'/api/getNowPlayingFilmList?cityId=320900&pageSize=10&pageNum=',

    // 即将上映
    getComingSoonFilmList:'/api/getComingSoonFilmList?cityId=320900&pageSize=10&pageNum=',


    // 电影详情
    getFilmInfo:'/api/getFilmInfo?filmId='

}