import moment from 'moment'

export default{
    fn1(value){
        return moment(value*1000).format('YYYY-MM-DD')
    },

    fn2(value){
        return moment(value*1000).format('YYYY-MM-DD HH')
    }


}