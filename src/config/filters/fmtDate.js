import Vue from 'vue'
// import moment from 'moment'

import method from './date'

// 全局过滤器
Vue.filter('frmDate',(value,type = 1)=>{
    // if(1 === type){
    //     return moment(value*1000).format('YYYY-MM-DD')
    // }

    return method['fn' + type](value)
})