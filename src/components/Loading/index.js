import Vue from 'vue'
import Loading from './Loading.vue'

// 全局组件
Vue.component('Loading',{
  render(h){
    return h(Loading)
  }
})