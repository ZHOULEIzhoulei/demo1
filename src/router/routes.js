// 引入模块化路由  
// 引入首页路由
import homeRouter from './routes/home'
// 引入商城路由
import mallRouter from './routes/mall'
// 引入资讯路由
import newsRouter from './routes/news'
// 引入购物车路由
import cartRouter from './routes/cart'
// 引入个人中心路由
import centerRouter from './routes/center'
// 引入公共路由
import publicRouter from './routes/public'

// 定义路由表  定义路由表，是数组形式，里面是一个个对象
// 匹配规则：从上向下，匹配到则停止向下
// 定义原则：精确写在上面，模糊写在下面
// 导出路由
export default [
  homeRouter,
  mallRouter,
  newsRouter,
  cartRouter,
  centerRouter,
//   ...  表示合并数组
  ...publicRouter
]