export default [
    {
      path: '/',
      redirect: '/films/nowplaying'
    },
    {
      path: '*',
      component: () => import(/*webpackChunkName:'notfound'*/'@/views/Notfound')
    }
  ]