// vueRouter是vue的一个插件，所以要先引入Vue类
import Vue from 'vue'
// 引入VueRouter类
import VueRouter from 'vue-router'
// 导入路由规则
import routes from './routes.js'


// 把VueRouter类添加到Vue中  插件
Vue.use(VueRouter)


// 实例化路由
const router = new VueRouter({
  // vue路由的模式， history|hash|abstract
  mode: process.env.NODE_ENV == 'dev' ? 'history' : 'hash',
  
  // 路由规则表配置
  routes
})

export default router
