import http from '../utils/Http'
import config from '../config/api/city'
/**
 * 获取城市列表
 * @return Promise
 */
//  export const getCitiesInfo = () => {
//     return http.get(config.getCitiesInfo)
//   }

export const getCitiesInfo = async () => {
  // 结构赋值
  let {data:{data:{cities}}} = await http.get(config.getCitiesInfo)
  // console.log(cities)
  
  // let rets = await http.get(config.getCitiesInfo)
  // console.log(ret.data.data.cities)

  // 字母索引集合 A-Z
  let indexCity = {}
  for(var i = 65; i<=90; i++){
    indexCity[String.fromCharCode(i)] = []
  }

  

  // // 对象的循环  第一种写法
  // Object.keys(indexCity).forEach(key=>{
  //   cities.filter(item=>{
  //     // substr(0,1)  提取字符串中的内容，从索引为的开始，截取一个

  //     // toLocaleLowerCase() 方法用于把字符串转换为小写
  //     if(item.pinyin.substr(0,1) == key.toLocaleLowerCase()){
  //       return true;
  //     }
  //   })
  // })

// 第二种写法
  Object.keys(indexCity).forEach(key=>{
    indexCity[key] = cities.filter(item => item.pinyin.substr(0,1) == key.toLocaleLowerCase())

    // 如果数组为空，则删除
    if(indexCity[key].length == 0) delete

    indexCity[key]
  })

  // console.log(indexCity)
  return indexCity

}



