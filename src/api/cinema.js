import http from '../utils/Http'
import config from '../config/api/cinema'
/**
 * 获取影院列表
 * @return Promise
 */
 export const getCinemaList = (page = 1) => {
    return http.get(config.getCinemaList + page)
  }
