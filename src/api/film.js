import http from '../utils/Http'
import config from '../config/api/film'
/**
 * 获取电影  正在热映
 * @return Promise
 */
 export const getNowPlayingFilmList = (page = 1) => {
    return http.get(config.getNowPlayingFilmList + page)
  }


  /**
 * 获取电影  即将上映
 * @return Promise
 */
 export const getComingSoonFilmList = (page = 1) => {
  return http.get(config.getComingSoonFilmList + page)
}
  /**
 * 根据电影id获取电影信息
 * @param {number} filmId
 * @return Promise
 */
 export const getFilmInfo = filmId => {
  return http.get(config.getFilmInfo + filmId)
}