import Vue from 'vue'
// 导入路由
// 实现跳转
import router from '../router'


// 跳转到  
// <mylink to='/'>aaa</mylink>
Vue.component('mylink',{
    props:{
        to:{
            type:String,
            require:true
        },
        // tag 这个可以写也可以不写
        tag:{
            type:String,
            // 这是个a标签
            default:'a'
        }
    },
    // 渲染
    render(h){
        // 匿名插槽
        let slot = this.$slots.default || '导航'

        return h(this.tag,{
            // 绑定点击事件
            on:{
                click:()=>{
                    router.push(this.to)
                }
            }
        },slot)
    }
})