import Vue from 'vue';

// // 导入所有vant组件  全局组件 导入后在项目中可以直接使用
// import Vant from 'vant';
// import 'vant/lib/index.css';

// Vue.use(Vant);


// 按需导入
import{List, Cell, Toast,IndexBar, IndexAnchor} from 'vant'

// // 引入

// 第一种写法
// Vue.use(List)
// Vue.use(Cell)
// Vue.use(Toast)
// Vue.use(IndexBar)
// Vue.use(IndexAnchor)


// 第二种写法
// const components = {List, Cell, Toast,IndexBar, IndexAnchor}

// for(let [, value] of Object.entries(components)){
//     Vue.use(value);
// }

// 第三种写法
const components = {List, Cell, Toast,IndexBar, IndexAnchor}

Object.values(components).forEach(value =>Vue.use(value))



