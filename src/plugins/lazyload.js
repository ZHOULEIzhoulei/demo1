import Vue from 'vue'
import VueLazyload from 'vue-lazyload'


Vue.use(VueLazyload, {
  preLoad: 1.3,
  //错误时现实的图片
  error: 'https://mall.s.maizuo.com/056b03f80ecfb34900d94c1c9a8f5ae6.jpg',
  //加载时显示的图片
  loading: 'https://mall.s.maizuo.com/056b03f80ecfb34900d94c1c9a8f5ae6.jpg',
  //尝试几次
  attempt: 3
})
 
